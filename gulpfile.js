// General 

var gulp            = require('gulp'),
    changed         = require('gulp-changed'),
    plumber         = require('gulp-plumber'),
    twig            = require('gulp-twig'),
    browser         = require('browser-sync'),
    sass            = require('gulp-sass'),
    minifyCSS       = require('gulp-cssnano'),
    autoprefixer    = require('gulp-autoprefixer'),
    sourcemaps      = require('gulp-sourcemaps'),


    src = {
        scss: 'dev/scss/**/*.scss',
        js: 'dev/js/*.js',
        twig: 'dev/*.html',
        twigs: 'dev/twig/**/*.twig',
        html: '*.html',
        css: 'build/css',
        map: '/maps',
        scripts: 'build/js'
    };

// Notifications
var notify          = require('gulp-notify');

// JS minifier
var uglify          = require('gulp-uglify');

// Makes the OS notification a bit more useful in case of error
var onError = function (err) {
  notify.onError({
    title: "gulp error in " + err.plugin,
    message: err.toString()
  })(err);
  this.emit('end');
};

// Compile Twig to HTML
gulp.task('twig', function () {
    return gulp.src(src.twig) // source
        .pipe(twig())
        .pipe(gulp.dest('build')); // output destination
});

gulp.task('sass', function () {
  gulp.src(src.scss) // source
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(changed(src.scss))
    .pipe(sourcemaps.init())
    .pipe(sass({errLogToConsole: true}))
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({browsers: ['last 2 versions']}))
    .pipe(sourcemaps.write(src.map))
    .pipe(gulp.dest(src.css)) // output
    .pipe(browser.reload({stream:true}))
});

gulp.task('js', function() {
  gulp.src(src.js)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(changed(src.js))
    .pipe(uglify())
    .pipe(gulp.dest(src.scripts)) // output
    .pipe(browser.reload({stream:true}))
});

// Browsersync
// server & file ward
gulp.task('pack', ['sass', 'twig', 'js'], function() {
  browser.init({
    server: {
      baseDir: "build",
    }
  });
  gulp.watch(src.scss, ['sass']);
  gulp.watch(src.js, ['js']);
  gulp.watch([src.twig, src.twigs], ['twig']).on('change', browser.reload);
});

// launcher
gulp.task('run', ['pack']);